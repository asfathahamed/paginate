(function() {

  function userCtrl($scope, $routeParams, userFactory) {
    $scope.pageNo = $routeParams.pageNo || 1 ;
    $scope.perPage = $routeParams.perPage || 3;

    userFactory.getUserList($scope.pageNo, $scope.perPage).then(function(res) {

      $scope.userListData = res.data.data;
      $scope.totalResult = res.data.total;
      // $scope.totalPage = res.data.total_pages; //This data from API is not correct
      $scope.totalPage = Math.floor($scope.totalResult/$scope.perPage) + ($scope.totalResult % $scope.perPage ? 1 : 0);

      $scope.urlArr = '';
      for(var i = 1; i <= $scope.totalPage; i++) {
        $scope.urlArr += "http://localhost:3000/#!/user/" + i + "/" +
        ($routeParams.perPage ? $routeParams.perPage : "");
        if(i != $scope.totalPage) $scope.urlArr +=  "|";
      };

    });

    $scope.openModal = function(id) {
      angular.element(id).modal();
    }

    function updateUser(data) {
      var len = $scope.userListData.length;
      $scope.userListData[len] = data;
    };

    $scope.addUser = function() {
      userFactory.addUser($scope.first_name, $scope.last_name)
      .then(function (data, status) {
        updateUser({
          first_name: data.data.first_name,
          last_name: data.data.last_name,
          id: data.data.id,
          avatar: 'http://www.gunstar.co.uk/images/user-placeholder.png'
        });
      })
    }
  };

  function userFactory($http) {
    return {
      getUserList: function(pageNo, perPage) {
        if(!!pageNo && !!perPage) {
          return $http.get("https://reqres.in/api/users?page="+pageNo+"&per_page="+perPage);
        } else if(!pageNo && !perPage) {
          return $http.get("https://reqres.in/api/users");
        } else {
          return $http.get("https://reqres.in/api/users?page="+pageNo);
        }
      },
      addUser: function(fName, lName) {
        return $http.post(
          "https://reqres.in/api/users",
          {
            first_name: fName,
            last_name: lName,
          }
        );
      }
    }
  };

  var appUser = angular.module('User', ['ngRoute', 'ang.pagination'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/user/:pageNo?/:perPage?', {
      templateUrl: 'templates/user-template.html',
    })
  }])
  .factory('userFactory', ['$http', userFactory])
  .controller('userListCtrl', ['$scope', '$routeParams','userFactory', userCtrl])
})();

var express = require('express');
var app = express();

app.use('/public', express.static(__dirname + '/bower_components'));
app.use('/public', express.static(__dirname + '/public'));
app.use('/public', express.static(__dirname + '/static'));
app.use('/public', express.static(__dirname + '/common'));
app.use('/modules', express.static(__dirname + '/modules'));
app.use('/components', express.static(__dirname + '/views/components'));
app.use('/templates', express.static(__dirname + '/views/templates'));

app.set('hostPort', '3000');
var hostPort = app.get('hostPort');
app.listen(hostPort, function() {
  console.log("Server running at http://localost:"+ hostPort +" !!");
});

app.get('/', function(req, res) {
  res.sendFile('index.html', {
    'root': __dirname + '/'
  });
});

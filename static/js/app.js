(function() {
  var app = angular.module('Paginate', ['ngRoute', 'User']);
  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({
      redirectTo: '/user'
    })
  }]);
})();

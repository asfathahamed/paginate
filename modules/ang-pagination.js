(function () {

  var dirTemplate = '<ul class="pagination">' +
    '<li><a href="#">Previous</a></li>' +
    '<li ng-class="{active: (currentPage == n)}" ng-repeat="n in range(totalPage)">'+
    '<a href="{{links[$index]}}">{{n}}</a></li>' +
    '<li><a href="#" style="color: #566670">Next</a></li>' +
    '</ul>';
  var app = angular.module('ang.pagination', [])
  .directive('angPagination', function() {
    var directive = {
      restrict: 'A',
      scope: {
        totalPage: "@",
        currentPage: "@",
        linksUrl: "@"
      },
      replace: true,
      template: dirTemplate,
      link: function(scope, telem, tattr) {
        scope.range = function(n) {
          var input = [];
          for (var i = 1; i <= n; i++) {
            input.push(i);
          }
          return input;
        };
        scope.links = scope.linksUrl.split("|");
        console.log(scope.links[0]);
      }
    }
    return directive;
  });
})();
